package it.aubay.posteitaliane.postepam;

import it.aubay.posteitaliane.concepts.PraticaBPM;

// classe per chiamare l'EJB che chiamer� i sistemi esterni
public class CallSystem {
	
	public boolean esitoCall = true;
	
	public CallSystem() {
		
	}
	
	public boolean execute(String sistemaDaChiamare, PraticaBPM praticaBPM) {
		
		try{
			// qua devo chiamare il client dell'EJB di Raffaele
			System.out.println("Sono entrato qua dentro per il sistema " + sistemaDaChiamare);
		}catch (Exception e){
			
			esitoCall = false;
		}
		
		return esitoCall;
	}

}
